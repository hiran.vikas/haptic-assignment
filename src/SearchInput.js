const SearchInput = (props) => {
    return(
        <input type="text" 
            className="inputSearch" 
            placeholder="Search Friend"
            onChange={props.onSearchText}/>
    )
}

export default SearchInput;
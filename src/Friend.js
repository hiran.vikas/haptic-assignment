import { useEffect, useState } from "react";

const Friend = (props) => {
    const [arr, setArr]=useState();
    useEffect(()=> {
        setArr(props.friendsArr);
    }, [])
    useEffect(()=> {
        console.log("use", props.friendsArr)
        setArr(props.friendsArr);
    }, [props.friendsArr])
    return(
        <div className="main">
          <div className="content">
              Friend List
          </div>
          <input type="text" 
            placeholder="Add you friend name and press enter" 
            className="frind_input"
            onKeyDown={props.handleKeyDown}/>
          
          {arr && arr.length > 0 && arr.map((friend, index) => {
              return(
                <div className="item" key={index}>
                    <div className="itemTitles">
                        <p className="title">{friend.name}</p>
                        <p className="subtitle">is your friend</p>
                    </div>
                    <div className="iconsDiv">
                        <button className="btn" 
                            title="favourite"
                            onClick={()=>props.favourite(friend.name)}>
                            {!friend.fav ? <i className="fa fa-star-o"></i>
                            : <i class="fa fa-star" aria-hidden="true" style={{color: "yellow"}}></i>}
                        </button>
                        <button className="btn trashbtn" 
                            title="delete" 
                            onClick={()=>props.deleteClicked(friend.name)}>
                            <i className="fa fa-trash"></i>
                        </button>
                    </div>
              </div>
              )
          })}

        </div>
      
    )
}

export default Friend;
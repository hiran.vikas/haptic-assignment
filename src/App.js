import './App.css';
import './styles/styles.css';
import Header from './Header';
import SearchInput from './SearchInput';
import Friend from './Friend';
import { useEffect, useState } from 'react';

function App() {
  const [friendsArr, setFriendsArr]= useState([{name: "Vikas Hiran", fav: false}, {name: "Haptic", fav: false}, {name: "John", fav: false}, {name: "Mahavir", fav: false}, {name: "Shifa", fav: false}, {name: "Rakesh", fav: false}, {name: "Codetoart", fav: false}, {name: "Pravin", fav: false}, {name: "Sam", fav: false}, {name: "Samdhan", fav: false}, {name: "OM", fav: false}, {name: "Pratik", fav: false}])
  const [friendsArr2, setFriendsArr2]=useState(friendsArr);

  const [pagedFriends, setPagedFriends]=useState([]);
  const [page, setPage]=useState(0);

  const FRINEDS_PER_PAGE = 4;

  useEffect(()=>{
    setPagedFriends(divideFriendsIntoPages(friendsArr2));
  }, [])

  const onSearchText = (e) => {
    const text = e.target.value;
    if(text.length > 0){
      const findArr = friendsArr.filter((s)=>s.name.toLowerCase().includes(text.toLowerCase())); 
      setPagedFriends(divideFriendsIntoPages(findArr))
    }
    else{ setFriendsArr2(friendsArr);
      setPagedFriends(divideFriendsIntoPages(friendsArr));
    }
  }

  const divideFriendsIntoPages = (friends => {
    const pagedFriends = [];
    [...Array(Math.ceil(friends.length/FRINEDS_PER_PAGE))].forEach((q, i) => {
      pagedFriends.push(friends.slice(0 + FRINEDS_PER_PAGE*i, FRINEDS_PER_PAGE + FRINEDS_PER_PAGE*i))
    })
    return pagedFriends;
  })

  const deleteClicked = (name) => {
    if(window.confirm(`Are you sure you want delete friend ${name}`)){
      const arr = friendsArr.filter((s)=>!s.name.includes(name));
      setFriendsArr2(arr);
      setFriendsArr(arr);
      setPagedFriends(divideFriendsIntoPages(arr));
    }
  }

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      console.log(event.target.value)
      if(event.target.value.trim().length > 0){
        const arr = [{name: event.target.value}, ...friendsArr];
        setFriendsArr(arr);
        setFriendsArr2(arr)
        setPagedFriends(divideFriendsIntoPages(arr));
      }
      else alert("Enter valid string")
    }
  }

  const favourite = (name) => {
    const arr = friendsArr.map((s)=>{
      if(s.name === name) return {...s, fav: !s.fav}
      else return s;
    });
    setFriendsArr2(arr);
    setFriendsArr(arr);
    setPagedFriends(divideFriendsIntoPages(arr));
  }

  const sortByFav = () => {
    const arr = [...friendsArr2].sort((a,b) => a.fav - b.fav);
    setFriendsArr2(arr);
    setFriendsArr(arr);
    setPagedFriends(divideFriendsIntoPages(arr));
  }

  const Pagination = ({pages, goTo}) => (
  <div>
    {pages.map((p, i) => (
        <button key={i} onClick={goTo} value={i}>{i+1}</button>
    ))}
  </div>
)
const goToPage = (evt) => {
  setPage(evt.target.value);
}

  return (
    <div>
      <Header />
      <div className="app">
        <div>
          <SearchInput onSearchText={onSearchText}/>
          <button className="btn" 
              style={{marginLeft: "8px"}}
              title="Sort by favourite"
              onClick={()=>{sortByFav()}}
            >
            Sort
          </button>
        </div>
        <Friend friendsArr={pagedFriends[page]}
          deleteClicked={deleteClicked}
          handleKeyDown={handleKeyDown}
          favourite={favourite}/>
        </div>
        <div style={{textAlign: "center", marginTop: "10px"}}>
        <Pagination pages={pagedFriends} goTo={goToPage} />
        </div>
    </div>
  );
}

export default App;
